package com.customer.api.customerapi.customer;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Set;


@Data @AllArgsConstructor @NoArgsConstructor @Builder
public class CustomerDTO {
    private Long id;
    private String firstName;
    private String lastName;
    private String idCode;
    private String customerCode;
    private String phoneNumber;
    private String mobileNumber;
    private String address;
    private Set<String> accountCode;
}
