package com.customer.api.customerapi.bootstrap;

import com.customer.api.customerapi.customer.CustomerDTO;
import com.customer.api.customerapi.customer.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.Set;
import java.util.TreeSet;

@Component
public class AddCustomerXp {
    private CustomerService customerService;

    @Autowired
    public AddCustomerXp(CustomerService customerService) {
        this.customerService = customerService;
    }

    @PostConstruct
    public void init(){
        Set<String> a1Code = new TreeSet<>();
        Set<String> a2Code = new TreeSet<>();
        a1Code.add("A01");
        a2Code.add("A02");
        CustomerDTO customer1 = new CustomerDTO(1L,"shahab","roghani","3860564757","234422","08138216335","09011264492","address",a1Code);
        CustomerDTO customer2 = new CustomerDTO(2L,"kamal","sabokroo","3860572910","543967","02133356783","09125668349","address",a2Code);
        customerService.addCustomer(customer1);
        customerService.addCustomer(customer2);
        Set<String> c1Code = new TreeSet<>();
        Set<String> c2Code = new TreeSet<>();
        c1Code.add("234422");
        c2Code.add("543967");
    }
}
