package com.customer.api.customerapi;

import java.util.List;

public interface DTOMapping<T,S> {
    S toDto(T t);
    List<S> toDto(List<T> t);
    T toEntity(S s);
    List<T> toEntity(List<S> s);
}
