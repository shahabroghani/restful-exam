package com.customer.api.customerapi.customer;

import com.customer.api.customerapi.DTOMapping;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
@Service
public class CustomerMapping implements DTOMapping<Customer,CustomerDTO> {
    private ModelMapper modelMapper;

    public CustomerMapping(ModelMapper modelMapper) {
        this.modelMapper = modelMapper;
    }

    @Override
    public CustomerDTO toDto(Customer customer) {
        return modelMapper.map(customer,CustomerDTO.class);
    }

    @Override
    public List<CustomerDTO> toDto(List<Customer> t) {
        List<CustomerDTO> customerDTOS = new ArrayList<>();
        t.forEach(a->customerDTOS.add(toDto(a)));
        return customerDTOS;
    }

    @Override
    public Customer toEntity(CustomerDTO customerDTO) {
        return modelMapper.map(customerDTO,Customer.class);
    }

    @Override
    public List<Customer> toEntity(List<CustomerDTO> customerDTOS) {
        List<Customer> customers = new ArrayList<>();
        customerDTOS.forEach(a->customers.add(toEntity(a)));
        return customers;
    }
}
