package com.customer.api.customerapi.customer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController @RequestMapping("/api/customer")
public class CustomerController {
    private CustomerService customerService;

    @Autowired
    public CustomerController(CustomerService customerService) {
        this.customerService = customerService;
    }

    @PostMapping
    public void addCustomer(@RequestBody CustomerDTO customerDTO){
        customerService.addCustomer(customerDTO);
    }

    @GetMapping("/{code}")
    public CustomerDTO findCustomer(@PathVariable String code){
        return customerService.find(code);
    }

    @GetMapping
    public List<CustomerDTO> findCustomers(){
        return customerService.findAll();
    }

}
