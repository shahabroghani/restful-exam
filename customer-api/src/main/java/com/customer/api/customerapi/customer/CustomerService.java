package com.customer.api.customerapi.customer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.Optional;

@Service
public class CustomerService {
    private CustomerRepository customerRepository;
    private CustomerMapping customerMapping;

    @Autowired
    public CustomerService(CustomerRepository customerRepository, CustomerMapping customerMapping) {
        this.customerRepository = customerRepository;
        this.customerMapping = customerMapping;
    }

    public void addCustomer(CustomerDTO customerDTO){
        Optional.of(customerDTO).map(customerMapping::toEntity).ifPresentOrElse(customerRepository::save,
                ()->{throw new RuntimeException("null object Customer!!");});
    }

    List<CustomerDTO> findAll(){
        List<Customer> customer = customerRepository.findAll();
        return customerMapping.toDto(customer);
    }

    CustomerDTO find(String code){
        Optional<Customer> customer = customerRepository.findCustomerByCustomerCode(code);
        return customerMapping.toDto(customer.orElseThrow());
    }
}
