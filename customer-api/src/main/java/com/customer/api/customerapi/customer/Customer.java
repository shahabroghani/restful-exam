package com.customer.api.customerapi.customer;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.Set;

@Entity @Data @AllArgsConstructor @NoArgsConstructor @Builder

public class Customer {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @NotBlank
    private String firstName;
    @NotBlank
    private String lastName;
    @NotBlank @Size(min = 10,max = 10)
    private String idCode;
    @NotBlank @Size(min = 6,max = 6)
    private String customerCode;
    @NotBlank @Size(min = 11,max = 11)
    private String phoneNumber;
    @NotBlank @Size(min = 11,max = 11)
    private String mobileNumber;
    @NotBlank
    private String address;
    @ElementCollection @CollectionTable
    private Set<String> accountCode;
}
