package com.account.api.accountapi.account;

import com.account.api.accountapi.DTOMapping;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
@Service
public class AccountMapping implements DTOMapping<Account,AccountDTO> {
    private ModelMapper modelMapper;

    public AccountMapping(ModelMapper modelMapper) {
        this.modelMapper = modelMapper;
    }

    @Override
    public AccountDTO toDto(Account account) {
        return modelMapper.map(account,AccountDTO.class);
    }

    @Override
    public List<AccountDTO> toDto(List<Account> accounts) {
        List<AccountDTO> dtos = new ArrayList<>();
        accounts.forEach(a->dtos.add(toDto(a)));
        return dtos;
    }

    @Override
    public Account toEntity(AccountDTO accountDTO) {
        return modelMapper.map(accountDTO,Account.class);
    }

    @Override
    public List<Account> toEntity(List<AccountDTO> accountDTOS) {
        List<Account> accounts = new ArrayList<>();
        accountDTOS.forEach(a->accounts.add(toEntity(a)));
        return accounts;
    }
}
