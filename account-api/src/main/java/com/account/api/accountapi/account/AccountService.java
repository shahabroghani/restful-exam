package com.account.api.accountapi.account;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Service
public class AccountService {
    private AccountRepository accountRepository;
    private AccountMapping accountMapping;

    @Autowired
    public AccountService(AccountRepository accountRepository,AccountMapping accountMapping) {
        this.accountRepository = accountRepository;
        this.accountMapping = accountMapping;
    }

    public void addAccount(AccountDTO accountDTO){
        Optional.of(accountDTO).map(accountMapping::toEntity).ifPresentOrElse(accountRepository::save,
                ()->{throw new RuntimeException("null object account!!");});
    }

    List<AccountDTO> findAll() {
        List<Account> accounts = accountRepository.findAll();
        return accountMapping.toDto(accounts);
    }

    AccountDTO find(String code) {
        Optional<Account> account = accountRepository.findAccountByCode(code);
        return accountMapping.toDto(account.orElseThrow());
    }

    void transaction(String code, Double amount){
        if (amount<0){
            Optional<Account> account = accountRepository.findAccountByCode(code);
            if (account.isPresent()){
                if (account.get().getBalance()+amount<0){
                    throw new RuntimeException("Amount not valid!");
                }
            }
        }
        accountRepository.findAccountByCode(code).ifPresentOrElse(
                a->{
                    a.setBalance(a.getBalance()+amount);
                    accountRepository.save(a);
                    },
                ()->{throw new RuntimeException("Account not found!");}
        );
    }

    @Transactional
    public void transport(String sender, String receiver, Double amount) {
        this.transaction(sender,-amount);
        this.transaction(receiver,amount);
    }


}
