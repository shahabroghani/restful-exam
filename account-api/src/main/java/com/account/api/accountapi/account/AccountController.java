package com.account.api.accountapi.account;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@RestController @RequestMapping("/api/account")
public class AccountController {
    private AccountService accountService;

    @Autowired
    public AccountController(AccountService accountService) {
        this.accountService = accountService;
    }

    @PostMapping
    public void addAccount(@RequestBody AccountDTO accountDTO){
        accountService.addAccount(accountDTO);
    }

    @GetMapping
    public List<AccountDTO> findAccounts(){
        return accountService.findAll();
    }

    @GetMapping("/{code}")
    public AccountDTO findAccount(@PathVariable String code){
        return accountService.find(code);
    }

    @PostMapping("/{code}/deposit")
    public void deposit(@PathVariable String code,@RequestParam(name = "amount") Double amount){
        accountService.transaction(code,amount);
    }

    @PostMapping("/{code}/withdraw")
    public void withdraw(@PathVariable String code,@RequestParam(name = "amount") Double amount){
        accountService.transaction(code,-amount);
    }

    @PostMapping("/transport")
    @ResponseStatus(HttpStatus.OK)
    public void transport(
            @RequestParam(name = "amount") Double amount,
            @RequestParam(name = "sender") String sender,
            @RequestParam(name = "receiver") String receiver){
        accountService.transport(sender,receiver,amount);
    }
}
