package com.account.api.accountapi.account;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.util.Set;

@Data @AllArgsConstructor @NoArgsConstructor
public class AccountDTO {
    private Long id;
    private String code;
    private Double balance;
    private Set<String> customerCode;
}
