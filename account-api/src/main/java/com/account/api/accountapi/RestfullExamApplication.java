package com.account.api.accountapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RestfullExamApplication {

    public static void main(String[] args) {
        SpringApplication.run(RestfullExamApplication.class, args);
    }

}
