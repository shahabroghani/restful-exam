package com.account.api.accountapi.bootstrap;

import com.account.api.accountapi.account.AccountService;
import com.account.api.accountapi.account.AccountDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.Set;
import java.util.TreeSet;

@Component
public class AddAccountXP {
    private AccountService accountService;

    @Autowired
    public AddAccountXP(AccountService accountService) {
        this.accountService = accountService;
    }

    @PostConstruct
    public void init(){
        Set<String> c1Code = new TreeSet<>();
        Set<String> c2Code = new TreeSet<>();
        c1Code.add("234422");
        c2Code.add("543967");
        AccountDTO account1 = new AccountDTO(null,"A01",10d,c1Code);
        AccountDTO account2 = new AccountDTO(null,"A02",20d,c2Code);
        accountService.addAccount(account1);
        accountService.addAccount(account2);
    }
}
